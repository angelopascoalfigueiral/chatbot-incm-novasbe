Article: 142, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825588/diploma/indice,
1. As disposições que de futuro se publicarem sobre matéria abrangida no presente Estatuto deverão, depois de ouvida, a 
administração da Caixa, ser nele inseridas no lugar próprio, por substituição, supressão ou adicionamento dos respetivos preceitos. 2. As taxas mencionadas no n.º 1 do artigo 93.º, no n.º 2 do artigo 95.º, no n.º 3 do artigo 104.º e no n.º 2 do artigo 107.º poderão 
ser revistas mediante portaria do Ministro das Finanças.