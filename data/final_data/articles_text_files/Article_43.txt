Article: 43, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825479/diploma/indice,
1. O regime da aposentação voluntária que não dependa de verificação de incapacidade fixa-se com base na lei em vigor à data em 
que seja recebido o pedido de aposentação pela CGA, sem prejuízo do disposto no n.º 7 do artigo 39.º, e na situação existente à 
data em que o mesmo seja despachado. 3. O disposto no n.º 1 não prejudica os efeitos que a lei atribua, em matéria de aposentação, a situações anteriores. 2. Nas restantes situações, o regime da aposentação fixa-se com base na lei em vigor e na situação existente à data em que: a) Seja
declarada a incapacidade pela competente junta médica ou homologado o parecer desta, quando a lei especial o exija; b) O 
interessado atinja o limite de idade; c) Se profira decisão que imponha pena expulsiva ou se prefira condenação penal definitiva da 
qual resulta a demissão ou que coloque o interessado em situação equivalente. 4. É irrelevante qualquer alteração de remunerações ocorrida posteriormente à data a que se refere o n.º 2 do artigo 33.º