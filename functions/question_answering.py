# Function to ask the model an individual question

def question_answering(query, pipeline, top_k_retriever=3, top_k_reader=5):

    from haystack_setup import haystack_setup
    
    # Run the model
    prediction = pipeline.run(query=query, top_k_retriever=top_k_retriever, top_k_reader=top_k_reader)
    
    return prediction