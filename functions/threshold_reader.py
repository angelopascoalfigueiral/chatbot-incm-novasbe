# Function to return the list of articles that surpass a given threshold of 'score' from a df with model predictions
# note: he group identified the optimal threshold to be 6, which is why it is default in the function

def threshold_reader(df, score_threshold=6):

    import pandas as pd
    import ast

    # Create the empty list to store the numbers of the articles that satisfy the threshold 
    pred_articles_list = []

    # Iterate through the complete dataframe of model predictions
    for query in df.index:
        
        temp_list = []
        # Iterate through the answers
        for answer in range(5): # 5 is the total number of predictions outputed by the model (top_k_reader), from which we select the over-threshold ones

            # if the model could not find an answer, NaN (float type) is returned, and the next answer must be considered
            if type(df.iloc[query][answer]) == float:
                pred_articles_list.append(temp_list)
                break

            # if the score of the answer is above the defined threshold, the corresponding article number is appended
            if ast.literal_eval(df.iloc[query][answer])['score'] >= score_threshold:
                temp_list.append(ast.literal_eval(df.iloc[query][answer])['meta']['article'])

            # when the lenght of the list with article numbers reaches 3, we stop appending, assuming INCM never wants to return more than 3 articles
            if len(temp_list) == 3:
                pred_articles_list.append(temp_list)
                break
                
            # when all answers have been evaluated, we append what we have from the temporary list to the predictions list
            if answer == 4:
                pred_articles_list.append(temp_list)

    return pred_articles_list