Article: 40, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825476/diploma/indice,
3. Se, porém, a eliminação for consequência de infração penal pela qual o ex-subscritor seja condenado a pena superior a dois 
anos, a concessão da pensão de aposentação apenas poderá ter lugar findo o cumprimento da pena, se contar 5 anos de serviço e
nos termos das alíneas a) ou b) do número anterior. 1. A eliminação da qualidade de subscritor não extingue o direito de requerer a aposentação nos seguintes casos: a) Previstos nos 
n.os 1 e 2 e nas alíneas a) e b) do n.º 3 do artigo 37.º, quando a cessação definitiva de funções ocorra após cinco anos de serviço; b) 
Previstos nos artigos 37.º-A e 37.º-B, quando a cessação definitiva de funções ocorra após cinco anos de subscritor e, 
cumulativamente, este não reúna as condições de acesso a pensão atribuída por outro regime de proteção social de inscrição 
obrigatória. 2. Quando a eliminação da qualidade de subscritor tiver resultado de demissão, mesmo com expresso fundamento em infração 
penal ou disciplinar, a aposentação só poderá ser concedida, a requerimento do interessado, dois anos após a aplicação da pena 
desde que ele conte, pelo menos, 5 anos de serviço e observada uma das seguintes condições: a) Seja declarado, em exame 
médico, absoluta e permanentemente incapaz; b) Tenha atingido o limite de idade.