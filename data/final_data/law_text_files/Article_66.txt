Article: 66, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825504/diploma/indice,
Os herdeiros do aposentado, no caso de falecimento deste, poderão obter a entrega das pensões em dívida, mediante o processo 
de habilitação previsto para os créditos sobre a Caixa Geral de Depósitos.