def xml_articles_to_txt(statute, articles_needed, create_files: bool = True):
        
    import xml.etree.ElementTree as ET
    
    # Parsing the given XML file
    tree = ET.parse(f'../../data/law_raw_data/{statute}.xml')

    # Getting the law text from the appropriate XML file section
    for node in tree.iter('TEXTO_ACTO'):
        
        statute_text = node.text

    # Going from a raw XML into lists of article texts and numbers
    split_txt = []
    for i in range(statute_text.count('\nArtigo')+1): # number of articles in the statute - Assuming "\nArtigo" is the split
        
        split_txt.append(statute_text.split('Artigo ')[i].split("\n"))


    articles_txt = []
    articles_nr = []
    for article in split_txt[1:]:
        
        articles_nr.append(article[0].split(".")[0]) # GET -A HERE
        
        articles_txt.append(article[2:-1])

    # only creates files in the destination folder if create_files is not set as False
    if create_files == True:
    
        # Writing the needed articles to a text file, with the statute name, article number, and article text
        if articles_needed == 'all': # if all articles are needed

            for article_nr, article_txt in zip(articles_nr, articles_txt):

                with open(f"../../data/final_data/external_statutes_text_files/{statute}-{article_nr}.txt","w+") as f: 

                    f.write(f"Statute: {statute}, Article: {article_nr}\n{' '.join(article_txt)}") 

        else: # if specific articles are needed

            # picks the text of the selected article numbers off the full list
            needed_txt = [articles_txt[index-1] for index in articles_needed]

            # picks the numbers of the selected article numbers off the full list
            needed_nrs = [articles_nr[index-1] for index in articles_needed]

            for article_nr, article_txt in zip(needed_nrs, needed_txt):

                with open(f"../../data/final_data/external_statutes_text_files/{statute}-{article_nr}.txt","w+") as f: 

                    f.write(f"Statute: {statute}, Article: {article_nr}\n{' '.join(article_txt)}")
                    
    else: # not creating files if the user specifies create_files to False
        pass
                
    
    # just for comparison purposes
    print('number of articles found:', statute_text.count('\nArtigo'))