Article: 110, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825552/diploma/indice,
Os processos podem ser consultados por advogado com procuração do interessado, durante o prazo para o recurso hierárquico 
necessário ou para o recurso contencioso.