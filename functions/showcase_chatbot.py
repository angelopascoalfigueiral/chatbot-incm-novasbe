from question_answering import *

def showchase_chatbot(pipeline, threshold = 6):
    """Showcases the chatbot in a simple way.
    Asks the user for a query and then retrives an answer
    (or various answers or even no answer) depending on the
    confidence of the model.

    Parameters
    ----------
    pipeline : haystack.pipeline.ExtractiveQAPipeline
        The pipeline with the data to retrieve (in this case,
        the pipeline with the articles)
    threshold : int, optional
        A threshold for chatbot score. It will only retrieve
        answers that have a superior score to the threshold's
        value.

    Returns
    -------
    print():
        Article Number of the answer
    print():
        Article link to DRE
    print():
        Confidence/Score of the model on that answer
    print():
        Text from the article that answers the question.

    """
    
    # Asking for an input
    question = input('Insira a sua pergunta:')

    # Using the question_answering function to get a response from the model
    prediction = question_answering(query=question, pipeline=pipeline, top_k_retriever=3, top_k_reader=5)

    # making a list out of the articles and appeding the article numbers of the answers
    answer_list = []
    for i in range(0,len(prediction['answers'])):
        answer_list.append(prediction['answers'][i]['meta']['article'])

    # Only unique values from articles
    answer_list = list(set(answer_list))

    # Iterate through each answer within each question
    try:
        for i in range(0,len(answer_list)):

            # Store the text of the full predicted article in a variable
            with open('../../data/final_data/articles_text_files/Article_' + answer_list[i] + '.txt', 'r') as file:
                pred_answer = file.readlines()[1:]
                score = prediction['answers'][i]['score']
            # if the score of the answer is superior to the threhsold
            if score >= threshold:
                print('\nArticle number:', prediction['answers'][i]['meta']['article'])
                print('Article link:', prediction['answers'][i]['meta']['link'])
                print('Score:', prediction['answers'][i]['score'])
                print(''.join(pred_answer).replace('\n', ''))

    # There are some articles that are in the validation set but were not included by DSKC as law articles in the original dataset. This bypasses the issue for the time-being          
    except OSError:
        pass
