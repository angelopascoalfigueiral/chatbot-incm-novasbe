Article: 59, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825496/diploma/indice,
A atualização das pensões será efetuada, em consequência da elevação geral dos vencimentos do funcionalismo ou da criação de 
suplemento ou subsídio geral sobre os mesmos, mediante diploma do Conselho de Ministros, sob proposta do Ministro das 
Finanças e do membro do Governo que tiver a seu cargo a função pública.