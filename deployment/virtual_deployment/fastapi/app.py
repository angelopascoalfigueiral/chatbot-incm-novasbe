#import encodings.idna
from haystack.retriever.sparse import TfidfRetriever
from fastapi import FastAPI, UploadFile
from fastapi.middleware.cors import CORSMiddleware
#from haystack.document_store.elasticsearch import ElasticsearchDocumentStore
#from haystack.retriever.sparse import ElasticsearchRetriever
from haystack.reader.farm import FARMReader
from haystack.pipeline import ExtractiveQAPipeline
import pandas as pd
from haystack.preprocessor.utils import convert_files_to_dicts, fetch_archive_from_http
from haystack.preprocessor.preprocessor import PreProcessor
import csv
import datetime

from haystack.document_store.memory import InMemoryDocumentStore
document_store = InMemoryDocumentStore()

df = pd.read_csv('complete_curated_law.csv')
dataframe_dic = {}
for column in df.columns:
    column_values = df[column]
    dataframe_dic[column] = column_values

# queries_df = pd.read_csv('queries.csv', header=None)

df.columns = ['article', 'art_title', 'full_descr', 'topic', 'link']
df['text'] = df['full_descr'].copy()
test = df.to_dict(orient='records')
preprocessor = PreProcessor(
    clean_empty_lines=True,
    clean_whitespace=True,
    clean_header_footer=False,
    split_by="word",
    split_length=200,
    split_respect_sentence_boundary=True
)
nested_docs = [preprocessor.process(d) for d in test]
docs = [d for x in nested_docs for d in x]
document_store.write_documents(docs)


#RETRIEVER = ElasticsearchRetriever(document_store=DOC_STORE)
RETRIEVER = TfidfRetriever(document_store=document_store)

# ''load Roberta reader from disk'''
READER = FARMReader(model_name_or_path="../bert_portuguese",
                    use_gpu=True, max_seq_len=500, doc_stride=5)
# /app/

# initialize pipeline
PIPELINE = ExtractiveQAPipeline(reader=READER, retriever=RETRIEVER)

# initialize API
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/api/')
async def hello():
    return "hello. Use site_link/api/query?q=reforma para fazer um query"


@app.get('/api/get_laws')
async def get_laws():
    df = pd.read_csv('complete_curated_law.csv')
    df_dic = df[['art_title', 'art_number', 'full_descr',
                 'link', 'topic']].to_dict('records')
    return df_dic


@app.get('/api/get_queries')
async def get_queries():
    queries_df = pd.read_csv('queries.csv', header=None)
    queries_dic = queries_df.sort_values(by=queries_df.columns[-2], ascending=False).to_dict('records')
    return queries_dic


@app.delete('/api/delete')
async def delete_article(art: str):
    df = pd.read_csv('complete_curated_law.csv')
    df = df[df['art_number'] != str(art)]
    df.to_csv('complete_curated_law.csv', index=False)


@app.get('/api/query')
async def get_query(q: str):
    """Makes query to doc store via Haystack pipeline.
    :param q: Query string representing the question being asked.
    :type q: str
    """

    # get answers
    question = q.replace('reforma', 'aposentação')
    predictions = PIPELINE.run(
        query=question, top_k_retriever=4, top_k_reader=4)

    # choose only some answers to be returned to users
    chosen_answers = []
    for prediction in predictions['answers']:
        if prediction['score'] >= 3:
            if len(chosen_answers) >= 1:

                articles = []
                for answer in chosen_answers:
                    articles.append(answer['meta']['article'])

                if prediction['meta']['article'] not in articles:
                    chosen_answers.append(prediction)
                # chosen_answers.append(prediction)
            else:
                chosen_answers.append(prediction)

    # write query to csv, with feedback 0 by default
    with open('queries.csv', 'a') as f:
        if len(chosen_answers) == 0:
            fields = [q, '-', '-', datetime.datetime.now(), 0]
            writer = csv.writer(f)
            writer.writerow(fields)

        else:
            for prediction in chosen_answers:
                fields = [q, prediction['meta']['article'], prediction['meta']['full_descr'], datetime.datetime.now(), 0]
                writer = csv.writer(f)
                writer.writerow(fields)

    # if there is no answer or only one of them with score > 3, then get 3 highest answers
    if len(chosen_answers) == 1:
        return [predictions['answers'][0]]
    elif len(chosen_answers) >= 3:
        return chosen_answers[:3]
    else:
        return chosen_answers


# Feedback
@app.get('/api/feedback')
async def get_feedback(q, art):
    queries_df = pd.read_csv('queries.csv', names=['q', 'art','answer', 'date', 'feedback'])
    queries_df['feedback'].loc[(queries_df['q'] == q) & (queries_df['art'] == art)]=1
    queries_df.to_csv('queries.csv', header=False, index=False)


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)

# 0.0.0.0
# '.us-east-2.compute.amazonaws.com'
# 127.0.0.1
