Article: 5, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825273/diploma/indice,
2. Havendo acumulação de cargos, a quota sobre a remuneração referida no n.º 1 será devida em relação: a) Ao cargo a que 
competir remuneração mais elevada ou, se as remunerações forem de igual montante, ao que houver determinado 
primeiramente a inscrição na Caixa; b) A todos os cargos acumulados, quando a lei permita a aposentação com base neles, 
simultaneamente, ou quando se trate de tempo não sobreposto. 3. A importância da quota será arredondada para número exato de escudos, por defeito, se a fracção for inferior a $50, e por 
excesso, se igual ou superior. 1. O subscritor contribuirá para a Caixa, em cada mês, com a quota de 6 por cento do total da remuneração que competir ao cargo 
exercido, em função do tempo de serviço prestado nesse mês. 