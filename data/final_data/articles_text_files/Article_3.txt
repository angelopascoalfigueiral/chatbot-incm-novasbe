Article: 3, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825271/diploma/indice,
1. A inscrição efetua-se mediante boletim, em duplicado, de modelo aprovado oficialmente, que o respetivo serviço preencherá e 
enviará à Caixa logo que o interessado entre em exercício de funções. 2. Se o subscritor passar a exercer funções em outro organismo ou serviço, sem interromper a inscrição, este enviará desde logo à 
Caixa, em duplicado, boletim complementar, de modelo oficialmente aprovado, contendo os dados relativos à nova situação.