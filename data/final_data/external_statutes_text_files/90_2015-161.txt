Statute: 90_2015, Article: 161
1 - O militar passa à situação de reforma, sem redução de pensão, sempre que:
a) Atinja os 66 anos de idade;
b) Complete, seguida ou interpoladamente, cinco anos na situação de reserva fora da efetividade de serviço, sem prejuízo do disposto no n.º 2;
c) Requeira a passagem à situação de reforma depois de completados 60 anos de idade.
2 - O militar, tendo prestado o tempo mínimo de serviço previsto no regime de proteção social aplicável, passa à situação de reforma sempre que:
a) Seja julgado física ou psiquicamente incapaz para todo o serviço, mediante parecer da junta médica do respetivo ramo, homologado pelo CEM após confirmação pela junta médica do regime de proteção social aplicável;
b) Opte pela colocação nesta situação quando se verifiquem as circunstâncias previstas na alínea a) do n.º 1 do artigo 150.º;
c) Seja abrangido por outras condições previstas na lei.
3 - No caso de militar abrangido pelo artigo 155.º, que transite para a situação de reserva com idade inferior ao limite de idade previsto no artigo 154.º, o tempo de permanência fora da efetividade de serviço, a que se refere a alínea b) do n.º 1, é contado a partir da data em que o militar atingir aquele limite de idade.
