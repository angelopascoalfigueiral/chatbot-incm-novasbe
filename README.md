# Chatbot INCM NovaSBE

This repository contains all stages of the construction of a Chatbot-like conversational tool for INCM (Imprensa Nacional - Casa da Moeda), by 4 Business Analytics students (Nova School of Business and Economics), on account of the PBL (Project-based Learning).


## Folder Content

#### data:
Contains all data documents, in different curation levels. While most documents are being used in other folders, there are some that have not  been necessary yet. Notably, the final_data folder contains the text data that is currently being fed to the Haystack framework to return answers to questions.

#### functions:
Contains the reused functions throughout the project (e.g., for data curation and performance estimation). Does not include BERT functions, as those are in the haystack folder. Will contain the functions for the future API that needs to be created for the back-office and chatbot platforms.

### haystack:
Contains the folders and files used by the Haystack framework used to run the BERT model in Portuguese. These files are currently being called exclusively in the notebooks folder, specifically in the model directory, but will be also used in the functions folder for the development of the API needed for deployment.

### notebooks:
Contains the operations performed to:

1. explore and curate the dataset;
2. train, run, and test the model;
3. develop and test evaluation metrics;
4. evaluate the answering accuracy against the validation dataset.


## How to run the model

1. Install all the needed requirements from the "requirements" file;
2. Navigate to the "haystak_setup" notebook, under the "model" folder;
3. Follow the setup steps in the notebook.  Hopefully, setup will be complete successfully;
4. Get an answer by writing a question on the query parameter (question_answering function) and run the cell;
5. You can change the top_k_retriever* and top_k_reader** parameters and check the differences.


*top_k_retriever defines how detailed the search should be: the higher, the better (but also the slower) your answers.

**top_k_reader defines how many answers the model should return in the output.



