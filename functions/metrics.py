def exact_match_metric(pred_articles_list, answers_list):
    '''
    if at least one article is in the list, we consider the answer to be right
    '''

    import ast
    
    # Initialize the counter for correct article numbers
    cnt = 0
    
    # Iterate through the predictions list
    for ind in range(len(pred_articles_list)):
        
        # if the model did not return any article for a given query (empty list), then do nothing
        if len(pred_articles_list[ind]) == 0:
            pass
        
        # if, for one question, at least one of the predicted article numbers matches any on the dataset validation, we add one correct prediction
        elif any((True for x in pred_articles_list[ind][0:len(pred_articles_list[ind])] if x in ast.literal_eval(answers_list[ind]))):
            cnt+=1
            
    em_accuracy = round(cnt/len(answers_list)*100, 2)
    
    return em_accuracy

# -----------------------------------------------------------------------------------------------------------------

def cosine_similarity_metric(pred_articles_list, answers_list, doc_dir='../../data/final_data/articles_text_files'):
    '''
    calculates the cosine similarity between the text of the real answer (validation dataset) and the predicted answer. Since, most often, the prediction is more than one article, this function calculates the largest cosine similarity out of the predictions and considers that as correct
    '''

    from sklearn.feature_extraction.text import TfidfVectorizer
    from sklearn.metrics.pairwise import cosine_similarity
    import numpy as np
    import ast

    # Initialize the dictionary that will contain the cosine similarity of each question in the validation dataset
    pred_score = {}

    # Convert the answers list from string of lists into lists 
    answer_list2 = []
    for i in answers_list:
        answer_list2.append(ast.literal_eval(i))

    # Iterate through each element of the predictions list (question)
    for ind in range(len(pred_articles_list)):

        # Get the index of the question being handled (1 up to length)
        pred_score[ind+1] = 0

        # Iterate through each answer within each question
        for pred in pred_articles_list[ind]:

            try:
                for answer in answer_list2[ind]:

                    # Store the text of the full predicted article in a variable
                    with open('../../data/final_data/articles_text_files/Article_' + pred + '.txt', 'r') as file:
                        pred_answer = file.readlines()[1:]

                    # Store the text of the full answer article (from the validation dataset) in a variable
                    with open('../../data/final_data/articles_text_files/Article_' + answer + '.txt', 'r') as file:
                        real_answer = file.readlines()[1:]

                    # Transform the text of both articles into vectors
                    vec = TfidfVectorizer()
                    X = vec.fit_transform([str(pred_answer), str(real_answer)])

                    # Calculate the cosine similarity between the two vectors
                    S = cosine_similarity(X)

                    # Get the largest cosine similarity out of all the predictions and store that one as the cosine similarity for that question
                    if round(S[1][0],2) >= pred_score[ind+1]:
                        pred_score[ind+1] = round(S[1][0],2)

            # There are some articles that are in the validation set but were not included by DSKC as law articles in the original dataset. This bypasses the issue for the time-being          
            except OSError:
                pass

    # Calucalte the cosine similarities within the dictionary
    cosine_similarity_value = round(np.array(list(pred_score.values())).mean() * 100, 2)
        
    return cosine_similarity_value