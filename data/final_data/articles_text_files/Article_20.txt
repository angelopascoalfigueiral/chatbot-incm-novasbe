Article: 20, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825450/diploma/indice,
A responsabilidade pelas importâncias referidas no artigo 18.º e pelas indemnizações previstas no n.º 3 do artigo 57.º, que se 
encontrem em dívida à Caixa, cessa com a definitiva eliminação do subscritor ou com a extinção da situação de aposentado. 