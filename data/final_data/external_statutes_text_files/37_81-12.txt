Statute: 37_81, Article: 12
Os efeitos das alterações de nacionalidade só se produzem a partir da data do registo dos actos ou factos de que dependem.
