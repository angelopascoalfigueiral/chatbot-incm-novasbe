Article: 132, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825578/diploma/indice,
1. O presente Estatuto entra em vigor em 1 de janeiro de 1973 e é aplicável, sem prejuízo do disposto no artigo 43.º, aos processos 
pendentes. 2. No caso de alteração de prazos em curso, observar-se-á o disposto na lei civil.