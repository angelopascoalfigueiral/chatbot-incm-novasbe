Statute: 230_93, Article: 1
O presente diploma declara a extinção da Guarda Fiscal (GF), cria, integrada na Guarda Nacional Republicana (GNR), a Brigada Fiscal (BF) e prevê as opções possíveis para o pessoal militar da extinta Guarda Fiscal de ingresso noutras instituições, para além da regra geral da integração na Guarda Nacional Republicana, estabelecendo o regime aplicável em cada caso.

