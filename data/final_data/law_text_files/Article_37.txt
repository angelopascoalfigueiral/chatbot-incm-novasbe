Article: 37, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825470/diploma/indice,
5. O tempo de inscrição nas instituições de previdência referidas no n.º 2 do artigo 4.º, quer anterior, quer posterior ao tempo de
inscrição na Caixa, conta-se também para o efeito de se considerar completado o prazo de garantia que resultar do disposto nos 
n.os 3 e 4. 2. A aposentação pode ainda verificar-se quando o subscritor atingir a idade pessoal de acesso à pensão de velhice, sendo esta a
que resulta da redução, por relação à idade normal de acesso à pensão de velhice em vigor, de quatro meses por cada ano civil que 
exceda os 40 anos de serviço efetivo à data da aposentação, não podendo a redução resultar no acesso à pensão antes dos 60 anos 
de idade. 4. O Governo poderá fixar, em diploma especial, limites de idade e de tempo de serviço inferiores aos referidos nos números 
anteriores, os quais prevalecerão sobre estes últimos. 3. Há ainda lugar a aposentação quando o subscritor, tendo, pelo menos, cinco anos de serviço: a) Seja declarado, em exame 
médico, absoluta e permanentemente incapaz para o exercício das suas funções; b) Atinja o limite de idade legalmente fixado para
o exercício das suas funções; c) Seja punido com pena expulsiva de natureza disciplinar ou, por condenação penal definitiva, 
demitido ou colocado em situação equivalente, sem prejuízo do disposto nos n.os 2 e 3 do artigo 40º. 1. A aposentação pode verificar-se, independentemente de qualquer outro requisito, quando o​ subscritor contar 15 anos de serviço 
e a idade normal de acesso à pensão de velhice que​ sucessivamente estiver estabelecida no regime geral de segurança social.​