# Function to get Haystack ready

def haystack_setup(doc_dir="../../data/final_data/articles_text_files"):
    
    # Import the needed modules
    from haystack.preprocessor.cleaning import clean_wiki_text
    from haystack.preprocessor.utils import convert_files_to_dicts, fetch_archive_from_http
    from haystack.reader.farm import FARMReader
    from haystack.reader.transformers import TransformersReader
    from haystack.utils import print_answers
    from haystack.document_store.elasticsearch import ElasticsearchDocumentStore
    from haystack.file_converter.txt import TextConverter
    from haystack.retriever.sparse import ElasticsearchRetriever
    from haystack.pipeline import ExtractiveQAPipeline
    import os 
            
    # Start our document storage in Elasticsearch
    document_store = ElasticsearchDocumentStore(host="localhost", username="", password="", index="document")
    
    # In order not to accumulate documents (resulting in repetitive answers), we need to delete the documents that were there
    document_store.delete_all_documents()

    # Set up the text converter
    converter = TextConverter(remove_numeric_tables=True, valid_languages=["pt","en"])

    # Update the "meta" property of the model to include the first line of each document
    docs = []
    with os.scandir(doc_dir) as it:
        
        # Iterating through the files in the folder
        for file in it:
            
            if file.name.endswith(".txt") and file.is_file():
                with open(file) as f:
                    first_line = f.readline().split(',') # reading the first line and split by the comma
                    article = first_line[0].split(':',1)[1].split(',', 1)[0] # getting the article number
                    link = first_line[1].split(':',1)[1] # getting the link
                    
                    # Updating the meta dictionary
                    cur_meta = {"article": article[1:], 'link': link}
                    
                    # Run the conversion on each file
                doc = converter.convert(file_path=f'{doc_dir}/{file.name}', meta=cur_meta)
                docs.append(doc)
    document_store.write_documents(docs)
    
    # Load our BERTIMBAU model (Brazilian-Portuguese model, from huggingface)
    retriever = ElasticsearchRetriever(document_store=document_store)
    reader = FARMReader(model_name_or_path="pierreguillou/bert-base-cased-squad-v1.1-portuguese", use_gpu=True)
    
    # Combine the reader and the retriver in order to run a prediction
    pipe = ExtractiveQAPipeline(reader, retriever)
    
    # Return the variable need to run a prediction
    return pipe