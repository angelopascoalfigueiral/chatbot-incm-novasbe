import React, {useEffect, useState} from 'react';
import './App.css';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
//   Put this in package json if not working //"proxy":"http://127.0.0.1:8000",


function createMarkup(answer) { return {__html: answer['meta'].full_descr}; };


function App() {
  const [question, setQuestion] = useState('')
  const [answers, setAnswers] = useState([])
  const [loading, setLoading] = useState(false)
  const [laws, setLaws] = useState([])
  const [showLaws, setShowLaws] = useState(false)
  const [queries, setQueries] = useState([])
  const [showQueries, setShowQueries] = useState(false)

  const handleSubmit = (event) => {
    event.preventDefault();

    const user_input = event.target.question; // question from the user
    setQuestion(user_input.value)
    console.log(user_input.value)
    setLoading(true)
    setAnswers([])
};

  const handleDelete = (event) => {
    event.preventDefault();

  const deleted_article = event.target.delete; // article from the user to delete
  console.log(deleted_article.value)
  fetch(`http://127.0.0.1:8000/api/delete?art=${deleted_article.value}`, {
    method: 'DELETE',
    body: {
     "art": deleted_article.value
    }
   });
  };

  const getQueries = () => {
    fetch(`http://127.0.0.1:8000/api/get_queries`).then(response => 
      response.json().then(data => {
      setQueries(data)
    }))
    // setShowQueries(!showQueries)
  }

  const showQueriesTable = () => {
    setShowQueries(!showQueries)
    getQueries()
  }

  
  const getLaws = () => {
    fetch(`http://127.0.0.1:8000/api/get_laws`).then(response => 
      response.json().then(data => {
      setLaws(data)
    }))
    setShowLaws(!showLaws)
  }

  useEffect(() => {
    fetch(`http://127.0.0.1:8000/api/query?q=${question}`).then(response => 
      response.json().then(data => {
      setAnswers(data)
      getQueries()
      setLoading(false)
      console.log(data)
    }))
  }, [question]
  );


//FEEDBACK
const upvote = (event_query) => {
  fetch(`http://127.0.0.1:8000/api/feedback?art=${event_query[1]}&q=${event_query[0]}`).then(response =>
  response.json().then(data => {
  getQueries()
  }))
  };


  // const upvote = (event) => {
  //   alert(event);
  // };

// const upvote = (event) => {
//   event.preventDefault();

//   const article = event.target.delete; // 
//   console.log(deleted_article.value)
//   fetch(`http://127.0.0.1:5000/api/delete?art=${deleted_article.value}`, {
//     method: 'DELETE',
//     body: {
//      "art": deleted_article.value
//     }
//    });
//   };

/* <form onSubmit={handleDelete}>
          <input type="text" name='delete' placeholder="Nº Artigo para apagar"/>
          <Button size="sm" variant="outline-primary" placeholder="Submeter" type="submit">Delete Law</Button>
        </form> */



  return (
    <div className='App'>
      <h1>Estatuto da Aposentação - Perguntas e Respostas</h1>
      <h3><center>versão 3.0</center></h3>
      <center>De momento, o utilizador desta plataforma de testes deve garantir que está a utilizar o browser Mozilla Firefox. Caso a resposta não seja retornada dentro de 2 segundos, a ligação está desativada.</center>
      <br></br>
      <center>Nota: A pesquisa deve ser feita através de questões completas (ex: "Quando me posso reformar antecipadamente") em vez de palavras-chave (ex: "reforma antecipada"). </center>
      <br></br>
      <br></br>
      <div className="Form">
        <form onSubmit={handleSubmit}>
          <input type="text" name='question' placeholder="Questão"/>
          <Button size="sm" variant="outline-primary" placeholder="Submeter" type="submit">Submeter</Button>
        </form>
        <br></br>
        <p>Questão: {question}</p>
        {(loading) ? <p>Loading...</p> : ''}
        {(console.log(answers))}
        {(answers.length !== 0) ? <div><p>Respostas:</p> {answers.map( (answer) => {
          return ( 
            <div>
              <p><a href={answer['meta'].link}>Estatuto da Aposentação Artigo {answer['meta'].article}.º</a> - <span class='withprewrap' dangerouslySetInnerHTML={createMarkup(answer)}/></p>
              <p>Mais informações:</p>
              <p><strong>Título do Artigo:</strong> {answer['meta'].art_title}</p>
              <p><strong>Tópico:</strong> {answer['meta'].topic}</p>
              <br></br><button onClick={() => upvote([question, answer['meta'].article])}>UPVOTE</button>
              <hr></hr>
            </div>
          );
          })}
          </div> : 'Não há respostas a apresentar.'}

        <br></br>
        <br></br>
        <Button variant="outline-primary" >Adicionar Leis</Button>
        <br></br>
        <br></br>

        <form onSubmit={handleDelete}>
          <input type="text" name='delete' placeholder="Nº Artigo para apagar"/>
          <Button size="sm" variant="outline-primary" placeholder="Submeter" type="submit">Delete Law</Button>
        </form>
        <br></br>
        <br></br>



        <Button variant="outline-primary" onClick={getLaws}>Mostrar Leis</Button>
        {console.log(laws)}
        {console.log(showLaws)}
        {(showLaws !== false) ? 
          <div>
            <Table striped bordered hover responsive sm>
              <thead>
                <tr>
                  <th>art_title</th>
                  <th>article</th>
                  <th>text</th>
                  <th>link</th>
                  <th>topic</th>
                </tr>
              </thead>
              <tbody> 
                  {
                    laws.map(item => {          
                      return (
                        <tr> 
                        {
                          Object.values(item).map(value => {
                            return (<td>{value}</td>)
                          })
                        } 
                        </tr>
                      );
                    })
                  }
              </tbody>
            </Table>
          </div>
        :
        <div></div>
        }
        <br></br>
        <Button variant="outline-primary" onClick={showQueriesTable} >Mostrar Queries</Button>
        {(showQueries !== false) ? 
          <div>
            <Table striped bordered hover responsive sm>
              <thead>
                <tr>
                  <th>Query</th>
                  <th>Artigo</th>
                  <th>Respostas</th>
                  <th>TimeStamp</th>
                  <th>Feedback</th>
                </tr>
              </thead>
              <tbody> 
                  {
                    queries.map(item => {          
                      return (
                        <tr> 
                        {
                          Object.values(item).map(value => {
                            return (<td>{value}</td>)
                          })
                        } 
                        </tr>
                      );
                    })
                  }
              </tbody>
            </Table>
          </div>
        :
        <div></div>
        }
         </div>
    </div>
  );
}

export default App;


// if (isLoggedIn) {    return <UserGreeting />;  }  return <GuestGreeting />;}