Article: 103, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825544/diploma/indice,
De quaisquer resoluções definitivas e executórias da administração da Caixa, ou tomadas por delegação sua, haverá recurso 
contencioso, nos termos gerais.