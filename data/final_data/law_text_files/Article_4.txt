Article: 4, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825272/diploma/indice,
1. A idade máxima para a inscrição na Caixa será a que corresponda à possibilidade de o subscritor perfazer o mínimo de 5 anos de 
serviço até atingir o limite de idade fixado por lei para o exercício do respetivo cargo. 5. Para efeitos do disposto na alínea d) do n.º 2, a pensão apenas é elevada para o montante mínimo legalmente previsto quando o
aposentado ou reformado não perceba pensão ou pensões de valor global igual ou superior à pensão mínima que seria devida com 
base exclusivamente no tempo de serviço da CGA, I. P. 3. Para efeitos do disposto no número anterior, consideram-se outros regimes de proteção social, o regime geral de segurança 
social, os regimes especiais de segurança social, os regimes das caixas de reforma ou previdência ainda subsistentes, o regime de 
segurança social substitutivo constante de instrumento de regulamentação coletiva de trabalho vigente no setor bancário e os 
regimes de segurança social estrangeiros ou internacionais, desde que confiram proteção nas eventualidades de invalidez e velhice.  4. Quando o cargo for exercido em regime de tempo parcial, será este considerado, só para efeitos de inscrição na Caixa, como 
tempo completo. 2. Os períodos contributivos cumpridos no âmbito de outros regimes de proteção social, na parte em que não se sobreponham aos 
períodos contributivos cumpridos no regime de proteção social convergente, são considerados e relevam para os seguintes efeitos:
a) Cumprimento do prazo de garantia; b) Condições de aposentação ou reforma; c) Determinação da taxa de bonificação; d) 
Apuramento da pensão mínima.