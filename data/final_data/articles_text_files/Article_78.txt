Article: 78, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825518/diploma/indice,
2. Não podem exercer atividade profissional remunerada nos termos do número anterior: a) Os aposentados e reformados que se 
tenham aposentado ou reformado com fundamento em incapacidade; b) Os aposentados e reformados por força de aplicação da 
pena disciplinar de aposentação ou reforma compulsiva. 4. A decisão de autorização do exercício de funções é precedida de proposta do membro do Governo que tenha o poder de direção, 
de superintendência, de tutela ou influência dominante sobre o serviço, entidade ou empresa onde as funções devam ser 
exercidas, e produz efeitos por um ano, exceto se fixar um prazo superior, em razão da natureza das funções. 3. Consideram-se abrangidos pelo conceito de atividade profissional remunerada: a) Todos os tipos de funções e de serviços, 
independentemente da sua duração ou regularidade; b) Todas as formas de contrapartida, pecuniária ou em espécie, direta ou 
indireta, da atividade desenvolvida, nomeadamente todas as prestações que, total ou parcialmente, constituem base de incidência 
contributiva nos termos do Código dos Regimes Contributivos do Sistema Previdencial da Segurança Social; c) Todas as modalidades
de contratos, independentemente da respetiva natureza, pública ou privada, laboral ou de aquisição de serviços. 1. Os aposentados, reformados, reservistas fora de efetividade e equiparados não podem exercer atividade profissional 
remunerada para quaisquer serviços da administração central, regional e autárquica, empresas públicas, entidades públicas 
empresariais, entidades que integram o setor empresarial regional e municipal e demais pessoas coletivas públicas, exceto quando
haja lei especial que o permita ou quando, por razões de interesse público excecional, sejam autorizados pelos membros do 
Governo responsáveis pelas áreas das finanças e da Administração Pública. 7. Os termos a que deve obedecer a autorização de exercício de funções prevista no n.º 1 pelos aposentados com recurso a mecanismos
legais de antecipação de aposentação são estabelecidos, atento o interesse público subjacente, por portaria dos membros do Governo 
responsáveis pelas áreas das finanças e da Administração Pública, sem prejuízo do disposto nos números anteriores. 5. (Revogado)
6. (Revogado)