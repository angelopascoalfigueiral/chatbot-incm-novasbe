Article: 76, Link: https://dre.pt/web/guest/legislacao-consolidada/-/lc/134309077/202012140343/73825516/diploma/indice,
1. Na aplicação de penas disciplinares aos aposentados, as de multa, suspensão ou inatividade serão substituídas pela perda da 
pensão de aposentação por igual tempo. 2. A pena de demissão ou equivalente determina a suspensão do abono da pensão pelo período de três anos.